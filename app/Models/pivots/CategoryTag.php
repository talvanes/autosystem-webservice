<?php

namespace AutoSystem\Models\pivots;

use Illuminate\Database\Eloquent\Model;

class CategoryTag extends Model
{
    /** @var string */
	protected $connection = 'pivots';
	/** @var string */
	protected $table = 'pivots.CategoryTag';
}
