<?php

namespace AutoSystem\Models\pivots;

use Illuminate\Database\Eloquent\Model;

class ModelTag extends Model
{
    /** @var string */
	protected $connection = 'pivots';
	/** @var string */
	protected $table = 'pivots.ModelTag';
}
