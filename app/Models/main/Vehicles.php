<?php

namespace AutoSystem\Models\main;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\main\VehicleModels;

class Vehicles extends Model
{
	/** @var string */
	protected $connection = 'main';
	/** @var string */
    protected $table = 'main.Vehicles';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'modelId', 'vinNumber', 'licensePlate', 'distanceRun', 
    	'year', 'price', 'history', 'colour', 'description',
    ];
    
    /** @var boolean */
    public $timestamps = true;
    /** @var boolean */
    public $incrementing = true;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    
    public function Model(){
    	return $this->belongsTo(VehicleModels::class, 'modelId');
    }
    
    public function Gallery(){
    	return $this->hasMany(VehicleThumbGallery::class, 'vehicleId');
    }
}
