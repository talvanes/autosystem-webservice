<?php

namespace AutoSystem\Models\main;

use Illuminate\Database\Eloquent\Model;

class VehicleThumbGallery extends Model
{
	/** @var string */
    protected $connection = 'main';
    /** @var string */
    protected $table = 'main.VehicleThumbGalleries';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'vehicleId', 'photoPath',
    ];
    
    /** @var boolean */
    public $incrementing = true;
    /** @var boolean */
    public $timestamps = true;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    public function Vehicle(){
    	return $this->belongsTo(Vehicles::class, 'vehicleId');
    }
}
