<?php

namespace AutoSystem\Models\main;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\main\VehicleModels;

class ReferencePrices extends Model
{
	/** @var string */
	protected $connection = 'main';
	/** @var string */
    protected $table = 'main.ReferencePrices';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'vehicleModelId', 'year', 'avgPrice',
    ];
    
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public $incrementing = true;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    public function Model(){
    	$this->belongsTo(VehicleModels::class, 'vehicleModelId');
    }
}
