<?php

namespace AutoSystem\Models\main;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\Manufacturers;
use AutoSystem\Models\config\VehicleCategories;
use AutoSystem\Models\config\FuelTypes;
use AutoSystem\Models\config\TransmissionTypes;
use AutoSystem\Models\config\DriveWheelTypes;
use AutoSystem\Models\config\Dimensions;
use AutoSystem\Models\config\VehicleTags;
use AutoSystem\Models\config\WheelDriveTypes;

class VehicleModels extends Model
{
	/** @var string */
	protected $connection = 'main';
	/** @var string */
    protected $table = 'main.VehicleModels';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'manufacturerId', 'categoryId', 'gearboxTransmissionId', 'wheelDriveTypeId', 'dimensionId',
    	'name', 'version', 'speeds', 'doors', 'wheels', 'isImported', 'isDiscontinued',
    	'thumbPath', 'width', 'height', 'length', 'wheelbase', 'weight', 'capacity',
    ];
    
    /** @var boolean */
    public $incrementing = true;
    /** @var boolean */
    public $timestamps = true;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    
    public function Manufacturer(){
    	return $this->belongsTo(Manufacturers::class, 'manufacturerId');
    }
    
    public function Category(){
    	return $this->belongsTo(VehicleCategories::class, 'categoryId');
    }
    
    /* public function Fuel(){
    	return $this->belongsTo(FuelTypes::class, 'fuelTypeId');
    } */
    
    public function Transmission(){
    	return $this->belongsTo(TransmissionTypes::class, 'transmissionTypeId');
    }
    
    public function WheelDrive(){
    	return $this->belongsTo(WheelDriveTypes::class, 'wheelDriveTypeId');
    }
    
    public function Size(){
    	return $this->belongsTo(Dimensions::class, 'dimensionId');
    }
    
    public function Tags(){
    	return $this->belongsToMany(VehicleTags::class, 'pivots.CategoryTag', 'vehicleTagId', 'vehicleModelId');
    }
    
}
