<?php

namespace AutoSystem\Models\main;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\VehicleTypes;
use AutoSystem\Models\config\FuelTypes;
use AutoSystem\Models\config\EngineAspirationTypes;
use AutoSystem\Models\config\EnginePositionTypes;
use AutoSystem\Models\config\EngineLayoutTypes;

class Engines extends Model
{
	/** @var string [description] */
    protected $connection = 'main';
    /** @var string [description] */
    protected $table = 'main.Engines';
    /** @var string [description] */
    protected $primaryKey = 'id';
    /** @var array [description] */
    protected $fillable = [
    	'name', 'torque', 'power', 'fuelTank', 'engineCapacity', 'cylinders', 'bore', 'stroke', 
    	'vehicleTypeId', 'fuelTypeId', 'engineAspirationId', 'enginePositionId', 'engineLayoutId',
    ];

    /** @var boolean [description] */
    public $timestamps = true;
    /** @var boolean [description] */
    public $incrementing = true;
    /** @var boolean [description] */
    public static $snakeAttributess = false;

    /**
     * [VehicleType description]
     */
    public function VehicleType() {
    	return $this->belongsTo(VehicleTypes::class, 'vehicleTypeId');
    }

    /**
     * [Fuel description]
     */
    public function Fuel() {
    	return $this->belongsTo(FuelTypes::class, 'fuelTypeId');
    }

    /**
     * [AspirationType description]
     */
    public function Aspiration() {
    	return $this->belongsTo(EngineAspirationTypes::class, 'engineAspirationId');
    }

    /**
     * [Position description]
     */
    public function Position() {
    	return $this->belongsTo(EnginePositionTypes::class, 'enginePositionId');
    }

    /**
     * [Layout description]
     */
    public function Layout() {
    	return $this->belongsTo(EngineLayoutTypes::class, 'engineLayoutId');
    }

}
