<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\VehicleTypes;
use AutoSystem\Models\config\VehicleTags;

class VehicleCategories extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.VehicleCategories';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'name', 'vehicleTypeId',
    ];
    
    /** @var boolean */
    public $incrementing = true; 
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Type(){
    	return $this->belongsTo(VehicleTypes::class, 'vehicleTypeId');
    }
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Tags(){
    	return $this->belongsToMany(VehicleTags::class, 'pivots.CategoryTag', 'vehicleTagId', 'vehicleCategoryId');
    }
}
