<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.Countries';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'id', 'name', 'flagPath',	
    ];
    
    /** @var boolean */
    public $incrementing = false;
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public static $snakeAttributes = false;
}
