<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\VehicleTypes;

class Dimensions extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.Dimensions';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'id', 'description', 'min', 'max', 'vehicleTypeId',	
    ];
    
    /** @var boolean */
    public $incrementing = false;
    /** @var string */
    public $timestamps = false;
    /** @var string */
    public static $snakeAttributes = false;
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function VehicleType(){
    	return $this->belongsTo(VehicleTypes::class, 'vehicleTypeId');
    }
}
