<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\main\VehicleModels;
use AutoSystem\Models\config\VehicleCategories;

class VehicleTags extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.VehicleTags';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [ 
    	'name',		
    ];
    
    /** @var boolean */
    public $incrementing = true;
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Categories(){
    	return $this->belongsToMany(VehicleCategories::class, 'pivots.CategoryTag', 'vehicleCategoryId', 'vehicleTagId');
    }
    
    public function Models(){
    	return $this->belongsToMany(VehicleModels::class, 'pivots.CategoryTag', 'vehicleModelId', 'vehicleTagId');
    }
}
