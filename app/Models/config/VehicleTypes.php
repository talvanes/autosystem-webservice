<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;

class VehicleTypes extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.VehicleTypes';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'name', 'description',	
    ];
    
    /** @var boolean */
    public $incrementing = true;
    /** @var string */
    public $timestamps = false;
    /** @var string */
    public static $snakeAttributes = false;
}
