<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;

class EngineAspirationTypes extends Model
{
    /** @var string [description] */
    protected $connection = 'config';
    /** @var string [description] */
    protected $table = 'config.EngineAspirationTypes';
    /** @var string [description] */
    protected $primaryKey = 'id';
    /** @var array [description] */
    protected $fillable = [
    	'id', 'description',
    ];

    /** @var boolean [description] */
    public $incrementing = false;
    /** @var boolean [description] */
    public $timestamps = false;
    /** @var boolean [description] */
    public static $snakeAttributes = false;
}
