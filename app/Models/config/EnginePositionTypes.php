<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;

class EnginePositionTypes extends Model
{
	/** @var string [description] */
    protected $connection = 'config';
    /** @var string [description] */
    protected $table = 'config.EnginePositionTypes';
    /** @var string [description] */
    protected $primaryKey = 'id';
    /** @var array [description] */
    protected $fillable = [
    	'id', 'description',
    ];

    /** @var boolean [description] */
    public $timestamps = false;
    /** @var boolean [description] */
    public $incrementing = false;
    /** @var boolean [description] */
    public static $snakeAttributes = false;
}
