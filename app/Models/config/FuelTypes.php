<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;

class FuelTypes extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.FuelTypes';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'id', 'description',
    ];
    
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public $incrementing = false;
    /** @var boolean */
    public static $snakeAttributes = false;
}
