<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\Countries;

class Manufacturers extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.Manufacturers';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'id', 'countryId', 'name', 'logo', 'status',	
    ];
    
    /** @var boolean */
    public $incrementing = false;
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Country(){
    	return $this->belongsTo(Countries::class, 'countryId');
    }
}
