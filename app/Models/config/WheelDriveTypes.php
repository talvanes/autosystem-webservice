<?php

namespace AutoSystem\Models\config;

use Illuminate\Database\Eloquent\Model;
use AutoSystem\Models\config\VehicleTypes;

class WheelDriveTypes extends Model
{
	/** @var string */
	protected $connection = 'config';
	/** @var string */
    protected $table = 'config.WheelDriveTypes';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $fillable = [
    	'id', 'description', 'vehicleTypeId', 'wheelDriveAlias',	
    ];
    
    /** @var boolean */
    public $incrementing = false;
    /** @var boolean */
    public $timestamps = false;
    /** @var boolean */
    public static $snakeAttributes = false;
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function VehicleType(){
    	return $this->belongsTo(VehicleTypes::class, 'vehicleTypeId');
    }
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Alias(){
    	return $this->belongsTo(WheelDriveTypes::class, 'wheelDriveAlias');
    }
}
