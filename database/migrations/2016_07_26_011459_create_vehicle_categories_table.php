<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.VehicleCategories', function (Blueprint $table) {
            # model category id
        	$table->smallIncrements('id');
        	
        	# model category name
        	$table->string('name');
        	
        	# model type id it references to (FK)
        	$table->unsignedSmallInteger('vehicleTypeId');
        	$table->foreign('vehicleTypeId')->references('id')->on('config.VehicleTypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.VehicleCategories');
    }
}
