<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotsModelTagPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivots.ModelTag', function (Blueprint $table) {
        	# 2 FK (e PK)
        	$table->unsignedSmallInteger('vehicleTagId');
            $table->unsignedBigInteger('vehicleModelId');
            # PK
            $table->primary(['vehicleTagId', 'vehicleModelId']);
            # FK
            $table->foreign('vehicleTagId')->references('id')->on('config.VehicleTags');
            $table->foreign('vehicleModelId')->references('id')->on('main.VehicleModels');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pivots.ModelTag');
    }
}
