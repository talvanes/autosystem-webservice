<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.Countries', function (Blueprint $table) {
        	# country id
            $table->string('id', 3);
            $table->primary('id');
            
            # name of the country
            $table->string('name')->nullable();
            
            # path to flag image
            $table->string('flagPath')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.Countries');
    }
}
