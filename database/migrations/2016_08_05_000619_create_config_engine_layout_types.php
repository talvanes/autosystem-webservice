<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigEngineLayoutTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.EngineLayoutTypes', function (Blueprint $table) {
            # engine layout type id
            $table->string('id');
            $table->primary('id');

            # engine layout description
            $table->string('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.EngineLayoutTypes');
    }
}
