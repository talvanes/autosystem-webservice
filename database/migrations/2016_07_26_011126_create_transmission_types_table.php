<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmissionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.TransmissionTypes', function (Blueprint $table) {
            # gearbox transmission type id
        	$table->string('id');
        	$table->primary('id');
        	
        	# description for gearbox transmission type
        	$table->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.TransmissionTypes');
    }
}
