<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigEngineAspirationTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.EngineAspirationTypes', function (Blueprint $table) {
            # aspiration type id
            $table->string('id');
            $table->primary('id');

            # aspiration type description
            $table->string('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.EngineAspirationTypes');
    }
}
