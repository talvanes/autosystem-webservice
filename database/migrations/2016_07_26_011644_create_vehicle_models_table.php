<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main.VehicleModels', function (Blueprint $table) {
            # vehicle model id
        	$table->bigIncrements('id');
        	
        	# vehicle model name
        	$table->string('name');
        	
        	# model version suffix
        	$table->string('version')->nullable();
        	
        	# number of speeds in the gearbox
        	$table->integer('speeds');
        	
        	# number of wheels
        	$table->integer('wheels')->default(4);
        	
        	# number of doors
        	$table->integer('doors')->default(2);
        	
        	# is it imported?
        	$table->boolean('isImported')->default(0);
        	# is it discontinued?
        	$table->boolean('isDiscontinued')->default(0);
        	
        	# vehicle model thumb (optional)
        	$table->string('thumbPath')->nullable();
        	
        	# some specs
        	$table->decimal('width', 10, 2)->nullable();
        	$table->decimal('height', 10, 2)->nullable();
        	$table->decimal('length', 10, 2)->nullable();
        	$table->decimal('wheelbase', 10, 2)->nullable();
        	$table->decimal('weight', 10, 2)->nullable();
        	$table->integer('capacity')->nullable();
        	
        	# Manufacturer (FK)
        	$table->string('manufacturerId', 3);
        	$table->foreign('manufacturerId')->references('id')->on('config.Manufacturers');
        	# Category (FK)
        	$table->unsignedSmallInteger('categoryId');
        	$table->foreign('categoryId')->references('id')->on('config.VehicleCategories');
        	# Gearbox Transmission Type (FK)
        	$table->string('gearboxTransmissionId');
        	$table->foreign('gearboxTransmissionId')->references('id')->on('config.TransmissionTypes');
        	# Drive Wheel Type (FK)
        	$table->string('wheelDriveTypeId');
        	$table->foreign('wheelDriveTypeId')->references('id')->on('config.WheelDriveTypes');
        	# Vehicle Model Size (FK)
        	$table->string('dimensionId');
        	$table->foreign('dimensionId')->references('id')->on('config.Dimensions');
        	
        	# vehicle model created at and/or updated at
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main.VehicleModels');
    }
}
