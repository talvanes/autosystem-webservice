<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.Dimensions', function (Blueprint $table) {
            # dimension identifier (subcompact, compact, medium-sized, big, etc)
        	$table->string('id');
            $table->primary('id');
            
            # dimension description
            $table->string('description')->nullable();
            
            # minimum dimension
            $table->decimal('min', 10, 2);
            
            # maximum dimension
            $table->decimal('max', 10, 2);
            
            # model type id (e.g. cars, motorcycles, trucks, etc) (FK)
            $table->unsignedSmallInteger('vehicleTypeId');
            $table->foreign('vehicleTypeId')->references('id')->on('config.VehicleTypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.Dimensions');
    }
}
