<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWheelDriveTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.WheelDriveTypes', function (Blueprint $table) {
            # drive wheel type id
        	$table->string('id');
        	$table->primary('id');
        	
        	# drive wheel type description
        	$table->string('description')->nullable();
        	
        	# vehicle model type id (car, motorcycle, truck, etc) (FK)
        	$table->unsignedSmallInteger('vehicleTypeId')->nullable();
        	$table->foreign('vehicleTypeId')->references('id')->on('config.VehicleTypes');
        	
        	# an alias for itself (e.g. AWD or 4x4 are the same as 4WD) (FK)
        	$table->string('wheelDriveAlias')->nullable();
        	$table->foreign('wheelDriveAlias')->references('id')->on('config.WheelDriveTypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.WheelDriveTypes');
    }
}
