<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main.Vehicles', function (Blueprint $table) {
            # vehicle id
        	$table->bigIncrements('id');
        	
        	# model-year id (this does not make much sense)
        	$table->unsignedBigInteger('modelId');
        	$table->foreign('modelId')->references('id')->on('main.VehicleModels');
        	
        	# vehicle year
        	$table->integer('year');
        	
        	# vehicle price
        	$table->decimal('price')->default(0.00);
        	
        	# vin number
        	$table->string('vinNumber', 17)->unique();
        	
        	# license plate
        	$table->string('licensePlate')->nullable();
        	
        	# kilometres/miles run
        	$table->integer('distanceRun')->default(0);
        	
        	# vehicle history
        	$table->text('history');
        	
        	# vehicle colour
        	$table->string('colour')->nullable();
        	
        	# vehicle description
        	$table->string('description')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main.Vehicles');
    }
}
