<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotsCategoryTagPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivots.CategoryTag', function (Blueprint $table) {
        	# 2 FK's (also PK's)
        	$table->unsignedSmallInteger('vehicleTagId');
        	$table->unsignedSmallInteger('vehicleCategoryId');
        	# PK
        	$table->primary(['vehicleTagId', 'vehicleCategoryId']);
        	# FK
        	$table->foreign('vehicleTagId')->references('id')->on('config.VehicleTags');
        	$table->foreign('vehicleCategoryId')->references('id')->on('config.VehicleCategories');
        	
        	# timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pivots.CategoryTag');
    }
}
