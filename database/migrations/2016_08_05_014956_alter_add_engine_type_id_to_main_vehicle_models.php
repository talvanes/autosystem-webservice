<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddEngineTypeIdToMainVehicleModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main.VehicleModels', function (Blueprint $table) {
            // add engine type FK
            $table->unsignedBigInteger('engineTypeId');
            $table->foreign('engineTypeId')->references('id')->on('main.Engines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main.VehicleModels', function (Blueprint $table) {
            $table->dropForeign(['engineTypeId']);
            $table->dropColumn('engineTypeId');
        });
    }
}
