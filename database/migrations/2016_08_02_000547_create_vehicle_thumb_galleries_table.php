<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleThumbGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main.VehicleThumbGalleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            # vehicle id
            $table->unsignedBigInteger('vehicleId');
            $table->foreign('vehicleId')->references('id')->on('main.Vehicles');
            
            # photo path
            $table->string('photoPath');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main.VehicleThumbGalleries');
    }
}
