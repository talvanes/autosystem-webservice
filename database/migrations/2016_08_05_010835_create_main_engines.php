<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainEngines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main.Engines', function (Blueprint $table) {
            # engine id
            $table->bigIncrements('id');

            # engine name: e.g. Tigershark 2.4 (Jeep Renegade), Pentastar V6 (Dodge Joruney, Jeep Cherokee and Fiat Freemont)
            $table->string('name');

            # torque (kgfm)
            $table->decimal('torque', 10, 2)->nullable();
            
            # power (HP, cc)
            $table->integer('power')->nullable();

            # fuel tank (l)
            $table->integer('fuelTank')->nullable();

            # engine capaaity (L) e.g. 1.0, 1.4, 1.6, 1.8, 2.0, 3.0, etc.
            $table->decimal('engineCapacity', 2, 1)->nullable();

            # number of cylinders
            $table->smallInteger('cylinders')->default(4);

            # bore and stroke (bore-stroke ratio)
            $table->decimal('bore')->nullable();
            $table->decimal('stroke')->nullable();

            # vehicle type which this engine is supposed to work into (FK)
            $table->unsignedSmallInteger('vehicleTypeId');
            $table->foreign('vehicleTypeId')->references('id')->on('config.VehicleTypes');

            # Fuel Type (FK)
            $table->string('fuelTypeId');
            $table->foreign('fuelTypeId')->references('id')->on('config.FuelTypes');

            # Aspiration Type (FK)
            $table->string('engineAspirationId');
            $table->foreign('engineAspirationId')->references('id')->on('config.EngineAspirationTypes');

            # Engine Position Type (FK)
            $table->string('enginePositionId');
            $table->foreign('enginePositionId')->references('id')->on('config.EnginePositionTypes');

            # Engine Layout Type (FK)
            $table->string('engineLayoutId');
            $table->foreign('engineLayoutId')->references('id')->on('config.EngineLayoutTypes');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main.Engines');
    }
}
