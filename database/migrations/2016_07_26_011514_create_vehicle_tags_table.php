<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.VehicleTags', function (Blueprint $table) {
            # vehicle model tag id
        	$table->smallIncrements('id');
        	
        	# vehicle model tag name
        	$table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.VehicleTags');
    }
}
