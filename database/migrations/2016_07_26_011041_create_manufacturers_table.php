<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.Manufacturers', function (Blueprint $table) {
        	# vehicle manufacturer id
            $table->string('id', 3);
            $table->primary('id');
            
            # country id (FK)
            $table->string('countryId');
            $table->foreign('countryId')->references('id')->on('config.Countries');
            
            # manufacturer name
            $table->string('name')->nullable();
            
            # path to manufacturer logo
            $table->string('logo')->nullable();
            
            # maunfacturer status (0-inactive, 1-active)
            $table->boolean('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.Manufacturers');
    }
}
