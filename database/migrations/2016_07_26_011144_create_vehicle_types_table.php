<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config.VehicleTypes', function (Blueprint $table) {
        	# vehicle model type id
            $table->smallIncrements('id');
            
            # vehicle model name
            $table->string('name');
            
            # vehicle model description
            $table->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config.VehicleTypes');
    }
}
