<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main.ReferencePrices', function (Blueprint $table) {
            # model-year id (this does not make much sense)
        	$table->bigIncrements('id');
        	
        	# vehicle model (FK)
        	$table->unsignedBigInteger('vehicleModelId');
        	$table->foreign('vehicleModelId')->references('id')->on('main.VehicleModels');
        	
        	# vehicle year
        	$table->integer('year');
        	
        	# vehicle price
        	$table->decimal('avgPrice', 10, 2)->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main.ReferencePrices');
    }
}
