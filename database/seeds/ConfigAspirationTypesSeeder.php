<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\EngineAspirationTypes;

class ConfigEngineAspirationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EngineAspirationTypes::create(['id' => 'ASP', 'description' => 'naturally-aspirated engines']);
        EngineAspirationTypes::create(['id' => 'TURBO', 'description' => 'turbocharged engines']);
        EngineAspirationTypes::create(['id' => 'SUPER', 'description' => 'supercharged engines']);
        EngineAspirationTypes::create(['id' => 'TWIN', 'description' => 'twincharged engines']);
    }
}
