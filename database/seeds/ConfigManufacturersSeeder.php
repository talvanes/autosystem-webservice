<?php
use Illuminate\Database\Seeder;
use AutoSystem\Models\config\Manufacturers;
class ConfigManufacturersSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$logoPath = storage_path ( 'logos' );
		
		// manufacturers' info
		Manufacturers::create ( [ 
				'id' => 'ALF',
				'name' => 'Alfa Romeo',
				'logo' => "{$logoPath}/alfaromeo.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ABA',
				'name' => 'Abarth',
				'logo' => "{$logoPath}/abarth.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'AGR',
				'name' => 'Agrale',
				'logo' => "{$logoPath}/agrale.png",
				'status' => 1,
				'countryId' => 'BRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ASM',
				'name' => 'Aston Martin',
				'logo' => "{$logoPath}/aston-martin.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'AUD',
				'name' => 'Audi',
				'logo' => "{$logoPath}/audi.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ARN',
				'name' => 'Arrinera',
				'logo' => "{$logoPath}/arrinera.png",
				'status' => 1,
				'countryId' => 'POL' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ACU',
				'name' => 'Acura',
				'logo' => "{$logoPath}/acura.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'AIX',
				'name' => 'Aixam',
				'logo' => "{$logoPath}/aixam.png",
				'status' => 1,
				'countryId' => 'FRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'AC',
				'name' => 'AC',
				'logo' => "{$logoPath}/ac.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BEN',
				'name' => 'Bentley',
				'logo' => "{$logoPath}/bentley.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BMW',
				'name' => 'BMW',
				'logo' => "{$logoPath}/bmw.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BUI',
				'name' => 'Buick',
				'logo' => "{$logoPath}/buick.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BUG',
				'name' => 'Bugatti',
				'logo' => "{$logoPath}/bugatti.png",
				'status' => 1,
				'countryId' => 'FRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CHV',
				'name' => 'Chevrolet',
				'logo' => "{$logoPath}/chevrolet.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CAD',
				'name' => 'Cadillac',
				'logo' => "{$logoPath}/cadillac.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CHR',
				'name' => 'Chrysler',
				'logo' => "{$logoPath}/chrysler.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CTR',
				'name' => 'Caterham',
				'logo' => "{$logoPath}/caterham.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'COV',
				'name' => 'Chevrolet Corvette',
				'logo' => "{$logoPath}/corvette.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CIT',
				'name' => 'Citroën',
				'logo' => "{$logoPath}/citroen.png",
				'status' => 1,
				'countryId' => 'FRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DAC',
				'name' => 'Dacia',
				'logo' => "{$logoPath}/dacia.png",
				'status' => 1,
				'countryId' => 'ROU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DAH',
				'name' => 'Daihatsu',
				'logo' => "{$logoPath}/daihatsu.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DDG',
				'name' => 'Dodge',
				'logo' => "{$logoPath}/dodge.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DAE',
				'name' => 'Daewoo',
				'logo' => "{$logoPath}/daewoo.png",
				'status' => 0,
				'countryId' => 'KOR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ELF',
				'name' => 'Elfin',
				'logo' => "{$logoPath}/elfin.png",
				'status' => 1,
				'countryId' => 'AUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'FIA',
				'name' => 'Fiat',
				'logo' => "{$logoPath}/fiat.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'FER',
				'name' => 'Ferrari',
				'logo' => "{$logoPath}/ferrari.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'FOR',
				'name' => 'Ford',
				'logo' => "{$logoPath}/ford.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GMC',
				'name' => 'GMC',
				'logo' => "{$logoPath}/gmc.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GAZ',
				'name' => 'GAZ',
				'logo' => "{$logoPath}/gaz.png",
				'status' => 1,
				'countryId' => 'RUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GIL',
				'name' => 'Gillet',
				'logo' => "{$logoPath}/gillet.png",
				'status' => 1,
				'countryId' => 'BEL' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GIN',
				'name' => 'Ginetta',
				'logo' => "{$logoPath}/ginetta.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GLY',
				'name' => 'Geely',
				'logo' => "{$logoPath}/geely.png",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GWL',
				'name' => 'Great Wall',
				'logo' => "{$logoPath}/great-wall.png",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'GMP',
				'name' => 'Gumpert',
				'logo' => "{$logoPath}/gumpert.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HND',
				'name' => 'Honda',
				'logo' => "{$logoPath}/honda.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [
				'id' => 'HOM',
				'name' => 'Honda Motorcycles',
				'logo' => "{$logoPath}/honda-moto.gif",
				'status' => 1,
				'countryId' => 'JPN'
		] );
		Manufacturers::create ( [ 
				'id' => 'HNS',
				'name' => 'Hennessey',
				'logo' => "{$logoPath}/hennessey.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HYN',
				'name' => 'Hyundai',
				'logo' => "{$logoPath}/hyundai.png",
				'status' => 1,
				'countryId' => 'KOR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HUM',
				'name' => 'Hummer',
				'logo' => "{$logoPath}/hummer.png",
				'status' => 0,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HOL',
				'name' => 'Holden',
				'logo' => "{$logoPath}/holden.png",
				'status' => 1,
				'countryId' => 'AUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ISZ',
				'name' => 'Isuzu',
				'logo' => "{$logoPath}/isuzu.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'INF',
				'name' => 'Infiniti',
				'logo' => "{$logoPath}/infinity.png",
				'status' => 1,
				'countryId' => 'HKG' 
		] );
		Manufacturers::create ( [ 
				'id' => 'JEE',
				'name' => 'Jeep',
				'logo' => "{$logoPath}/jeep.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'JAG',
				'name' => 'Jaguar',
				'logo' => "{$logoPath}/jaguar.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'JOS',
				'name' => 'Joss',
				'logo' => "{$logoPath}/joss.png",
				'status' => 1,
				'countryId' => 'AUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'KMZ',
				'name' => 'Kamaz',
				'logo' => "{$logoPath}/kamaz.png",
				'status' => 1,
				'countryId' => 'RUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'KNS',
				'name' => 'Koenigsegg',
				'logo' => "{$logoPath}/koenigsegg.png",
				'status' => 1,
				'countryId' => 'SWE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LMB',
				'name' => 'Lamborghini',
				'logo' => "{$logoPath}/lamborghini.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LNR',
				'name' => 'Land Rover',
				'logo' => "{$logoPath}/land-rover.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LAN',
				'name' => 'Lancia',
				'logo' => "{$logoPath}/lancia.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LNC',
				'name' => 'Lincoln',
				'logo' => "{$logoPath}/lincoln.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LEX',
				'name' => 'Lexus',
				'logo' => "{$logoPath}/lexus.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LXG',
				'name' => 'Luxgen',
				'logo' => "{$logoPath}/luxgen.png",
				'status' => 1,
				'countryId' => 'TWN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LOT',
				'name' => 'Lotus',
				'logo' => "{$logoPath}/lotus.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LAD',
				'name' => 'Lada',
				'logo' => "{$logoPath}/lada.png",
				'status' => 1,
				'countryId' => 'RUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MAR',
				'name' => 'Maruti Suzuki',
				'logo' => "{$logoPath}/maruti-suzuki.png",
				'status' => 1,
				'countryId' => 'IND' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MAS',
				'name' => 'Maseratti',
				'logo' => "{$logoPath}/maseratti.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MAH',
				'name' => 'Mahindra',
				'logo' => "{$logoPath}/mahindra.png",
				'status' => 1,
				'countryId' => 'IND' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MAZ',
				'name' => 'Mazda',
				'logo' => "{$logoPath}/mazda.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MAY',
				'name' => 'Maybach',
				'logo' => "{$logoPath}/maybach.png",
				'status' => 0,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MCL',
				'name' => 'McLaren',
				'logo' => "{$logoPath}/mclaren.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MRC',
				'name' => 'Mercedes Benz',
				'logo' => "{$logoPath}/mercedes-benz.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MIN',
				'name' => 'MINI',
				'logo' => "{$logoPath}/mini.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MOR',
				'name' => 'Morgan Motor',
				'logo' => "{$logoPath}/morgan-motor.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MIT',
				'name' => 'Mitsubishi',
				'logo' => "{$logoPath}/mitsubishi.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MUS',
				'name' => 'Ford Mustang',
				'logo' => "{$logoPath}/mustang.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MOS',
				'name' => 'Mosler',
				'logo' => "{$logoPath}/mosler.png",
				'status' => 0,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'NIS',
				'name' => 'Nissan',
				'logo' => "{$logoPath}/nissan.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'NOB',
				'name' => 'Noble',
				'logo' => "{$logoPath}/noble.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'OPL',
				'name' => 'Opel',
				'logo' => "{$logoPath}/open.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PEU',
				'name' => 'Peugeot',
				'logo' => "{$logoPath}/peugeot.png",
				'status' => 1,
				'countryId' => 'FRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PAG',
				'name' => 'Pagani',
				'logo' => "{$logoPath}/pagani.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PIA',
				'name' => 'Piaggio',
				'logo' => "{$logoPath}/piaggio.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PNZ',
				'name' => 'Panoz',
				'logo' => "{$logoPath}/panoz.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PRS',
				'name' => 'Porsche',
				'logo' => "{$logoPath}/porsche.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PRD',
				'name' => 'Perodua',
				'logo' => "{$logoPath}/perodua.png",
				'status' => 1,
				'countryId' => 'MYS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PNF',
				'name' => 'Pininfarina',
				'logo' => "{$logoPath}/pininfarina.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'PRO',
				'name' => 'Proton',
				'logo' => "{$logoPath}/proton.png",
				'status' => 1,
				'countryId' => 'MYS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'REN',
				'name' => 'Renault',
				'logo' => "{$logoPath}/renault.png",
				'status' => 1,
				'countryId' => 'FRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'RRS',
				'name' => 'Rolls Royce',
				'logo' => "{$logoPath}/rolls-royce.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ROV',
				'name' => 'Rover',
				'logo' => "{$logoPath}/rover.png",
				'status' => 0,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'RMC',
				'name' => 'Rimac',
				'logo' => "{$logoPath}/rimac.png",
				'status' => 1,
				'countryId' => 'HRV' 
		] );
		Manufacturers::create ( [ 
				'id' => 'RUF',
				'name' => 'RUF',
				'logo' => "{$logoPath}/ruf.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'REV',
				'name' => 'Reva',
				'logo' => "{$logoPath}/reva.png",
				'status' => 1,
				'countryId' => 'IND' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SCA',
				'name' => 'Scania',
				'logo' => "{$logoPath}/scania.png",
				'status' => 1,
				'countryId' => 'SWE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SEA',
				'name' => 'Seat',
				'logo' => "{$logoPath}/seat.png",
				'status' => 1,
				'countryId' => 'ESP' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SAA',
				'name' => 'Saab',
				'logo' => "{$logoPath}/saab.png",
				'status' => 1,
				'countryId' => 'SWE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SCI',
				'name' => 'Scion',
				'logo' => "{$logoPath}/scion.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SKD',
				'name' => 'Škoda',
				'logo' => "{$logoPath}/skoda.png",
				'status' => 1,
				'countryId' => 'CZE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SPK',
				'name' => 'Spyker',
				'logo' => "{$logoPath}/spyker.png",
				'status' => 1,
				'countryId' => 'NLD' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SMR',
				'name' => 'Smart',
				'logo' => "{$logoPath}/smart.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SHL',
				'name' => 'Shelby Mustang',
				'logo' => "{$logoPath}/shelby.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SBR',
				'name' => 'Subaru',
				'logo' => "{$logoPath}/subaru.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SSY',
				'name' => 'SsangYong',
				'logo' => "{$logoPath}/ssangyong.png",
				'status' => 1,
				'countryId' => 'KOR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SUZ',
				'name' => 'Suzuki',
				'logo' => "{$logoPath}/suzuki.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SSC',
				'name' => 'SSC North America',
				'logo' => "{$logoPath}/ssc.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TSL',
				'name' => 'Tesla',
				'logo' => "{$logoPath}/tesla.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TAT',
				'name' => 'Tata Motors',
				'logo' => "{$logoPath}/tata.png",
				'status' => 1,
				'countryId' => 'IND' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TOY',
				'name' => 'Toyota',
				'logo' => "{$logoPath}/toyota.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TTR',
				'name' => 'Tatra',
				'logo' => "{$logoPath}/tatra.png",
				'status' => 1,
				'countryId' => 'CZE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'THK',
				'name' => 'Think',
				'logo' => "{$logoPath}/think.png",
				'status' => 0,
				'countryId' => 'NOR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TRL',
				'name' => 'Troller',
				'logo' => "{$logoPath}/troller.png",
				'status' => 1,
				'countryId' => 'BRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TVR',
				'name' => 'TVR',
				'logo' => "{$logoPath}/tvr.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TRM',
				'name' => 'Tramontana',
				'logo' => "{$logoPath}/tramontana.png",
				'status' => 1,
				'countryId' => 'ESP' 
		] );
		Manufacturers::create ( [ 
				'id' => 'UAZ',
				'name' => 'UAZ',
				'logo' => "{$logoPath}/uaz.png",
				'status' => 1,
				'countryId' => 'RUS' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VLK',
				'name' => 'Volkswagen',
				'logo' => "{$logoPath}/volkswagen.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VND',
				'name' => 'Vandenbrink',
				'logo' => "{$logoPath}/vandenbrink.png",
				'status' => 1,
				'countryId' => 'NLD' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VOL',
				'name' => 'Volvo',
				'logo' => "{$logoPath}/volvo.png",
				'status' => 1,
				'countryId' => 'SWE' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VAX',
				'name' => 'Vauxhall',
				'logo' => "{$logoPath}/vauxhall.png",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VEC',
				'name' => 'Vector Motors',
				'logo' => "{$logoPath}/vector-motors.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VEN',
				'name' => 'Venturi',
				'logo' => "{$logoPath}/venturi.png",
				'status' => 1,
				'countryId' => 'MCO' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VIP',
				'name' => 'Dodge Viper',
				'logo' => "{$logoPath}/viper.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'WIE',
				'name' => 'Wiesmann',
				'logo' => "{$logoPath}/wiesmann.png",
				'status' => 1,
				'countryId' => 'DEU' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ZAZ',
				'name' => 'ZAZ',
				'logo' => "{$logoPath}/zaz.png",
				'status' => 1,
				'countryId' => 'UKR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ZAG',
				'name' => 'Zagato',
				'logo' => "{$logoPath}/zagato.png",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ZIL',
				'name' => 'ZiL',
				'logo' => "{$logoPath}/zil.png",
				'status' => 1,
				'countryId' => 'RUS' 
		] );
		
		// some more car brands (e.g. car manufacturers from China)
		Manufacturers::create ( [ 
				'id' => 'CHY',
				'name' => 'Chery',
				'logo' => "{$logoPath}/chery.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'JAC',
				'name' => 'JAC Motors',
				'logo' => "{$logoPath}/jac.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BRL',
				'name' => 'Brilliance',
				'logo' => "{$logoPath}/brilliance.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BYD',
				'name' => 'BYD Auto',
				'logo' => "{$logoPath}/byd.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HGQ',
				'name' => 'Hongqi',
				'logo' => "{$logoPath}/hongqi.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ASI',
				'name' => 'ASIA Motors',
				'logo' => "{$logoPath}/asia.png",
				'status' => 0,
				'countryId' => 'KOR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'LFN',
				'name' => 'Lifan Motors',
				'logo' => "{$logoPath}/lifan.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'ZTY',
				'name' => 'Zotye',
				'logo' => "{$logoPath}/zotye.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DAF',
				'name' => 'DAF',
				'logo' => "{$logoPath}/daf.jpg",
				'status' => 1,
				'countryId' => 'NLD' 
		] );
		Manufacturers::create ( [ 
				'id' => 'EFF',
				'name' => 'Effa Motors',
				'logo' => "{$logoPath}/effa.jpg",
				'status' => 1,
				'countryId' => 'URY' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HAF',
				'name' => 'Hafei Motors',
				'logo' => "{$logoPath}/hafei.jpg",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SHR',
				'name' => 'Shineray',
				'logo' => "{$logoPath}/shineray.png",
				'status' => 1,
				'countryId' => 'CHN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'SND',
				'name' => 'Sundown Motos',
				'logo' => "{$logoPath}/sundown.jpg",
				'status' => 1,
				'countryId' => 'BRA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DFR',
				'name' => 'Dafra',
				'logo' => "{$logoPath}/dafra.gif",
				'status' => 1,
				'countryId' => 'BRA' 
		] );
		
		
		Manufacturers::create ( [
				'id' => 'MAN',
				'name' => 'MAN',
				'logo' => "{$logoPath}/man.png",
				'status' => 1,
				'countryId' => 'DEU'
		] );
		Manufacturers::create ( [
				'id' => 'FOT',
				'name' => 'Foton',
				'logo' => "{$logoPath}/foton.jpg",
				'status' => 1,
				'countryId' => 'CHN'
		] );
		Manufacturers::create ( [
				'id' => 'TRX',
				'name' => 'Traxx',
				'logo' => "{$logoPath}/traxx.gif",
				'status' => 1,
				'countryId' => 'CHN'
		] );
		Manufacturers::create ( [
				'id' => 'IVC',
				'name' => 'Iveco',
				'logo' => "{$logoPath}/iveco.jpg",
				'status' => 1,
				'countryId' => 'ITA'
		] );
		
		
		Manufacturers::create ( [ 
				'id' => 'APR',
				'name' => 'Aprilia',
				'logo' => "{$logoPath}/aprilia.jpeg",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'BUL',
				'name' => 'Buell',
				'logo' => "{$logoPath}/buell.jpeg",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'DUC',
				'name' => 'Ducatti',
				'logo' => "{$logoPath}/ducatti.jpeg",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'CAN',
				'name' => 'Can-Am',
				'logo' => "{$logoPath}/can-am.jpeg",
				'status' => 1,
				'countryId' => 'CAN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'HAR',
				'name' => 'Harley Davidson',
				'logo' => "{$logoPath}/harley-davidson.jpeg",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'IND',
				'name' => 'Indian',
				'logo' => "{$logoPath}/indian.png",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'KWS',
				'name' => 'Kawasaki',
				'logo' => "{$logoPath}/kawasaki.jpeg",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'KTM',
				'name' => 'KTM',
				'logo' => "{$logoPath}/ktm.jpeg",
				'status' => 1,
				'countryId' => 'AUT' 
		] );
		Manufacturers::create ( [ 
				'id' => 'KYC',
				'name' => 'KymCo',
				'logo' => "{$logoPath}/kymco.jpeg",
				'status' => 1,
				'countryId' => 'TWN' 
		] );
		Manufacturers::create ( [ 
				'id' => 'MGU',
				'name' => 'Moto Guzzi',
				'logo' => "{$logoPath}/moto-guzzi.jpeg",
				'status' => 1,
				'countryId' => 'ITA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'TRI',
				'name' => 'Triumph',
				'logo' => "{$logoPath}/triumph.jpeg",
				'status' => 1,
				'countryId' => 'GBR' 
		] );
		Manufacturers::create ( [ 
				'id' => 'VIC',
				'name' => 'Victory',
				'logo' => "{$logoPath}/victory.jpeg",
				'status' => 1,
				'countryId' => 'USA' 
		] );
		Manufacturers::create ( [ 
				'id' => 'YAH',
				'name' => 'Yamaha',
				'logo' => "{$logoPath}/yamaha.png",
				'status' => 1,
				'countryId' => 'JPN' 
		] );
	}
}
