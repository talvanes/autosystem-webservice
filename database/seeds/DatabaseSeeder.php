<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigCountriesSeeder::class);
        $this->call(ConfigManufacturersSeeder::class);
        $this->call(ConfigVehicleFuelTypesSeeder::class);
        $this->call(ConfigGearboxTransmissionTypesSeeder::class);
        $this->call(ConfigVehicleTypesSeeder::class);
        $this->call(ConfigWheelDriveSeeder::class);
        $this->call(ConfigVehicleCategoriesSeeder::class);
        $this->call(ConfigEngineAspirationTypesSeeder::class);
        $this->call(ConfigEnginePositionTypesSeeder::class);
        $this->call(ConfigEngineLayoutTypesSeeder::class);
    }
}
