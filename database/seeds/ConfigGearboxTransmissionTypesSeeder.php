<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\TransmissionTypes;

class ConfigGearboxTransmissionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransmissionTypes::create([ 'id' => 'MAN', 'description' => 'manual transmission' ]);
        
        TransmissionTypes::create([ 'id' => 'AUTO', 'description' => 'automatic transmission' ]);
        
        TransmissionTypes::create([ 'id' => 'CVT', 'description' => 'continuous variable transmission' ]);
        
        TransmissionTypes::create([ 'id' => 'S-AUTO', 'description' => 'semi-automatic transmission' ]);
        
        TransmissionTypes::create([ 'id' => 'DSG', 'description' => 'direct shift gearbox' ]);
        
        // common in Volkswagen models
        TransmissionTypes::create([ 'id' => 'TIP', 'description' => 'TipTronic(R) transmission' ]);
    }
}
