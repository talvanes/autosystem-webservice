<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\VehicleCategories;

class ConfigVehicleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*// 1 - car
        VehicleCategories::create([ 'name' => 'hatchback', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'sedan', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'station wagon', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'coupe', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'convertible', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'sport car', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'minivan', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'SUV', 'vehicleTypeId' => '1' ]);
        
        // 2 - motorcycle
        VehicleCategories::create([ 'name' => 'street', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'touring', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'cruiser', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'sport', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'standard', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'scooter', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'dual-purpose', 'vehicleTypeId' => '2' ]);
        VehicleCategories::create([ 'name' => 'off-road', 'vehicleTypeId' => '2' ]);
        
        // 3 - moped
        VehicleCategories::create([ 'name' => 'moped', 'vehicleTypeId' => '3' ]);
        
        // 4 - light truck
        VehicleCategories::create([ 'name' => 'pickup truck', 'vehicleTypeId' => '4' ]);
        VehicleCategories::create([ 'name' => 'cargo van', 'vehicleTypeId' => '4' ]);
        
        // 5 - truck
        VehicleCategories::create([ 'name' => 'mini truck', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'light duty', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'medium duty', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'heavy duty', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'very heavy truck', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'transporter', 'vehicleTypeId' => '5' ]);
        
        // 6 - bus
        VehicleCategories::create([ 'name' => 'bus', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'minibus', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'passenger van', 'vehicleTypeId' => '6' ]);*/

        // 1 - Passenger cars: hatchbacks, sedans, station wagons, coupes, convertibles and sport cars
        VehicleCategories::create([ 'name' => 'hatchback', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'sedan', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'station wagon', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'coupe', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'convertible', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create([ 'name' => 'sport car', 'vehicleTypeId' => '1' ]);
        VehicleCategories::create(['name' => 'cargo van', 'vehicleTypeId' => '8']);

        // 2 - Buggy cars
        VehicleCategories::create([ 'name' => 'buggy', 'vehicleTypeId' => '2' ]);
        

        // 3 - Multipurpose Vehicles (MPV's): minivans
        VehicleCategories::create([ 'name' => 'minivan', 'vehicleTypeId' => '3' ]);
        

        // 4 - Sport Utility Vehicles (SUV's): small (compact), medium, heavy, luxury
        VehicleCategories::create([ 'name' => 'compact SUV', 'vehicleTypeId' => '4' ]);
        VehicleCategories::create([ 'name' => 'medium SUV', 'vehicleTypeId' => '4' ]);
        VehicleCategories::create([ 'name' => 'heavy SUV', 'vehicleTypeId' => '4' ]);
        VehicleCategories::create([ 'name' => 'luxury SUV', 'vehicleTypeId' => '4' ]);
        

        // 5 - Pickup Trucks: single cabin. single extended cabin, double cabin
        VehicleCategories::create([ 'name' => 'single cabin', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'single extended cabin', 'vehicleTypeId' => '5' ]);
        VehicleCategories::create([ 'name' => 'double cabin', 'vehicleTypeId' => '5' ]);
        

        // 6 - Motorcycles: 4-wheel, street, touring, cruiser, sport, standard, dual-purpose, off-road and scooters
        VehicleCategories::create([ 'name' => 'street', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'touring', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'cruiser', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'sport', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'standard', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'dual-purpose', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'off-road', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => '4-wheel', 'vehicleTypeId' => '6' ]);
        VehicleCategories::create([ 'name' => 'scooter', 'vehicleTypeId' => '6' ]);
        

        // 7 - Mopeds
        VehicleCategories::create([ 'name' => 'moped', 'vehicleTypeId' => '7' ]);
        

        // 8 - Trucks: e.g. cargo vans, tractor trailers and transporters
        VehicleCategories::create(['name' => 'mini truck', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'single unit (2 axis)', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'single unit (3 or more axis)', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'truck tractor', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'trailer truck', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'tractor + semi-trailer', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'double trailer truck', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'triple trailer truck', 'vehicleTypeId' => '8']);
        VehicleCategories::create(['name' => 'transporter', 'vehicleTypeId' => '8']);


        // 9 - Buses: buses, minibuses and passenger vans
        VehicleCategories::create(['name' => 'minibus', 'vehicleTypeId' => '9']);
        VehicleCategories::create(['name' => 'bus', 'vehicleTypeId' => '9']);
        VehicleCategories::create(['name' => 'passenger van', 'vehicleTypeId' => '9']);

        // 10 - Trailers
        VehicleCategories::create(['name' => 'conventional travel trailer', 'vehicleTypeId' => '10']);
        VehicleCategories::create(['name' => 'fifth-wheel travel trailer', 'vehicleTypeId' => '10']);
        VehicleCategories::create(['name' => 'travel trailer with expendable ends', 'vehicleTypeId' => '10']);
        VehicleCategories::create(['name' => 'folding camping trailer', 'vehicleTypeId' => '10']);
        VehicleCategories::create(['name' => 'truck camper', 'vehicleTypeId' => '10']);
        VehicleCategories::create(['name' => 'sport utility RVS', 'vehicleTypeId' => '10']);

        // 11 - Semi trailers
        VehicleCategories::create(['name' => 'water tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'dump', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'oil tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'chemical tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'heating asphalt tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'refreigerator van', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'van', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'bulk cement tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'concrete mixer', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'LPG tank', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'flat bed', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'low bed', 'vehicleTypeId' => '11']);
        VehicleCategories::create(['name' => 'skeleton container', 'vehicleTypeId' => '11']);

        // 12 - Motor Homes
        VehicleCategories::create(['name' => 'type A', 'vehicleTypeId' => '12']);
        VehicleCategories::create(['name' => 'type B', 'vehicleTypeId' => '12']);
        VehicleCategories::create(['name' => 'type C', 'vehicleTypeId' => '12']);

    }
}
