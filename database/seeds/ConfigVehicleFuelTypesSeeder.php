<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\FuelTypes;

class ConfigVehicleFuelTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FuelTypes::create(['id' => 'GAS', 'description' => 'gasoline']);
        
        FuelTypes::create(['id' => 'DIE', 'description' => 'diesel']);
        
        FuelTypes::create(['id' => 'LPETR', 'description' => 'liquified petroleum']);
        
        FuelTypes::create(['id' => 'CNG', 'description' => 'compressed natural gas']);
        
        FuelTypes::create(['id' => 'ETH', 'description' => 'ethanol']);
        
        FuelTypes::create(['id' => 'B-DIE', 'description' => 'bio-diesel']);
        
        FuelTypes::create(['id' => 'B-ETH', 'description' => 'bio-ethanol']);
        
        FuelTypes::create(['id' => 'ELEC', 'description' => 'electric']);
        
        FuelTypes::create(['id' => 'HYB', 'description' => 'hybrid']);
        
        // Brazil only
        FuelTypes::create(['id' => 'FLEX', 'description' => 'flex (gasoline + ethanol)']);
    }
}
