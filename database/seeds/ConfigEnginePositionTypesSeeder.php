<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\EnginePositionTypes;

class ConfigEnginePositionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EnginePositionTypes::create(['id' => 'F', 'description' => 'front position']);
        EnginePositionTypes::create(['id' => 'M', 'description' => 'mid position']);
        EnginePositionTypes::create(['id' => 'R', 'description' => 'rear position']);
    }
}
