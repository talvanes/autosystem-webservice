<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\VehicleTypes;

class ConfigVehicleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*VehicleTypes::create(['name' => 'car', 'description' => 'Passenger cars: hatchbacks, sedans, station wagons, minivans, SUVs, coupes, convertibles and sport cars']);
        VehicleTypes::create(['name' => 'motorcycle', 'description' => 'Motorcycles: 4-wheel, street, touring, cruiser, sport, standard, dual-purpose, off-road and scooters']);
        VehicleTypes::create(['name' => 'moped', 'description' => 'Mopeds']);
        VehicleTypes::create(['name' => 'light truck', 'description' => 'Light trucks: pickup trucks and cargo vans']);
        VehicleTypes::create(['name' => 'truck', 'description' => 'Trucks: mini trucks, medium and heavy duty trucks (includes transporters)']);
        VehicleTypes::create(['name' => 'bus', 'description' => 'Bus, minibus and passenger vans']);*/

        VehicleTypes::create(['name' => 'car', 'description' => 'Passenger cars']);
        VehicleTypes::create(['name' => 'buggy', 'description' => 'Buggy cars']);
        VehicleTypes::create(['name' => 'MPV', 'description' => "Multipurpose Vehicles (MPV's)"]);
        VehicleTypes::create(['name' => 'SUV', 'description' => "Sport Utility Vehicles (SUV's)"]);
        VehicleTypes::create(['name' => 'pickup', 'description' => 'Pickup Trucks']);
        VehicleTypes::create(['name' => 'motorcycle', 'description' => 'Motorcycles and scooters']);
        VehicleTypes::create(['name' => 'moped', 'description' => 'Mopeds']);
        VehicleTypes::create(['name' => 'truck', 'description' => 'Trucks']);
        VehicleTypes::create(['name' => 'bus', 'description' => 'Buses']);
        VehicleTypes::create(['name' => 'trailer', 'description' => 'Trailers']);
        VehicleTypes::create(['name' => 'semitrailer', 'description' => 'Semitrailers']);
        VehicleTypes::create(['name' => 'motorhome', 'description' => 'Motor Homes']);

    }
}
