<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\EngineLayoutTypes;

class ConfigEngineLayoutTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EngineLayoutTypes::create(['id' => 'W', 'description' => 'W engines']);
        EngineLayoutTypes::create(['id' => 'V', 'description' => 'V engines']);
        EngineLayoutTypes::create(['id' => 'IN', 'description' => 'inline engines']);
        EngineLayoutTypes::create(['id' => 'B', 'description' => 'boxer engines']);
        EngineLayoutTypes::create(['id' => 'V6', 'description' => 'VR-6 engines']);
        EngineLayoutTypes::create(['id' => 'WK', 'description' => 'wankel engines']);
    }
}
