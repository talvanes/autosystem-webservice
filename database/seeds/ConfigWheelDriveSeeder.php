<?php

use Illuminate\Database\Seeder;
use AutoSystem\Models\config\WheelDriveTypes;

class ConfigWheelDriveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Main wheel drive types (all vehicles)
        WheelDriveTypes::create([
        	'id' => 'FWD', 
        	'description' => 'front wheel drive', 
        	'vehicleTypeId' => NULL, 
        	'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => 'RWD',
        		'description' => 'rear wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => 'AWD',
        		'description' => 'all-wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => 'IWD',
        		'description' => 'individual wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => NULL,
        ]);
        
        WheelDriveTypes::create([
        		'id' => '2WD',
        		'description' => 'two wheel drive (either front or rear)',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => '4WD',
        		'description' => 'four wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => 'AWD',
        ]);
        
        // Wheel drive types for motorcycles
        WheelDriveTypes::create([
        		'id' => '2x1',
        		'description' => 'two-by-one wheel drive (motorcycle only)',
        		'vehicleTypeId' => 6,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => '2x2',
        		'description' => 'two-by-two wheel drive (motorcycle only)',
        		'vehicleTypeId' => 6,
        		'wheelDriveAlias' => 'AWD',
        ]);
        
        // Wheel drive types for cars and light trucks
        WheelDriveTypes::create([
        		'id' => '4x2',
        		'description' => 'four-by-two wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => '2WD',
        ]);
        WheelDriveTypes::create([
        		'id' => '4x4',
        		'description' => 'four-by-four wheel drive',
        		'vehicleTypeId' => NULL,
        		'wheelDriveAlias' => '4WD',
        ]);
        
        // Wheel drive types for trucks //
        WheelDriveTypes::create([
        		'id' => '6WD',
        		'description' => 'six wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => 'AWD',
        ]);
        WheelDriveTypes::create([
        		'id' => '6x6',
        		'description' => 'six-by-six wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => '6WD',
        ]);
        
        WheelDriveTypes::create([
        		'id' => '8WD',
        		'description' => 'eight wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => 'AWD',
        ]);
        WheelDriveTypes::create([
        		'id' => '8x8',
        		'description' => 'eight-by-eight wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => '8WD',
        ]);
        
        WheelDriveTypes::create([
        		'id' => '12WD',
        		'description' => 'twelve wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => 'AWD',
        ]);
        WheelDriveTypes::create([
        		'id' => '12x12',
        		'description' => 'twelve-by-twelve wheel drive (truck only)',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => '12WD',
        ]);
        
        // Other wheel drive types
        WheelDriveTypes::create([
        		'id' => '6x2',
        		'description' => 'six-by-two wheel drive',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => '6x4',
        		'description' => 'six-by-four wheel drive',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => NULL,
        ]);
        
        WheelDriveTypes::create([
        		'id' => '8x4',
        		'description' => 'eight-by-four wheel drive',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => NULL,
        ]);
        WheelDriveTypes::create([
        		'id' => '8x6',
        		'description' => 'eight-by-six wheel drive',
        		'vehicleTypeId' => 8,
        		'wheelDriveAlias' => NULL,
        ]);
        
    }
}
